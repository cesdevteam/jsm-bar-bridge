*-----------------------------------------------------------*
* Function: OpenFile										*
* Author: Colin Richardson									*
*-----------------------------------------------------------*
* Opens up a Table											*
*-----------------------------------------------------------*
* PARAMETERS: lcLocation - What location is it in?			*
*			  lnArea - In which area should it be opened	*
*			  lcOrder - Which order to open it with?		*
*			  lcAlias - What alias should it be called?		*
*			  llExclusive - Open it Exclusive or Shared?	*
*			  llReOpen - Reopen this file again?			*
*-----------------------------------------------------------*
* Modification Summary
*
* /01   Nick Rowlands 07/06/2007
*		Added a check to see if the table was opened using 
*		the requested path.
*		
*
*-------------------------------------------------------------

FUNCTION OpenFile

LPARAMETER lcTable, lcLocation, lnArea, lcOrder, lcAlias, llExclusive, llReOpen

LOCAL llWasntUNC, lcDataPath, lcFilePath

llWasntUNC = .F.

IF VARTYPE(lcOrder) != 'C'
	lcOrder = ""
ENDIF	

IF VARTYPE(lnArea) != 'N'
	lnArea = 0
ENDIF	

IF VARTYPE(lcAlias) != 'C' OR EMPTY(lcAlias)
	lcAlias = lcTable
ENDIF	

IF VARTYPE(lcLocation) != 'C'
	lcLocation = 'S'
ENDIF	

IF lcLocation = 'U'
	lcLocation = 'S'
ENDIF


*!*/01 NR 07/06/2007  Added check to make sure table is opened with the correct path 
*!* Moved this so that the we have the relevant path info. 
DO CASE
	CASE UPPER(lcLocation) = 'L'
		lcFilePath = goApp.cLocalPath
	CASE UPPER(lcLocation) = 'S'
		lcFilePath = goApp.cServerPath
	CASE UPPER(lcLocation) = 'A'
		lcFilePath = goApp.cAccountsPath
	CASE UPPER(lcLocation) = 'R'
		lcFilePath = goApp.cRemotePath
	CASE UPPER(lcLocation) = 'H'
		lcFilePath = goApp.cHomePath	
	OTHERWISE
		lcFilePath = goApp.cServerPath
ENDCASE

*** Check to see if Table is already open ***
IF USED(lcAlias) 
	SELECT (lcAlias)
	IF EMPTY(lcOrder) = .F.
		SET ORDER TO (lcOrder)
	ENDIF	
	lcDataPath = DBF() 
	IF lcFilePath $lcDataPath && The Requested File is open with the requested path. 
		RETURN .T.
	ELSE 
		USE IN (lcAlias)	&& Close the file as it is not open using the requested path. 
	ENDIF
ENDIF	
*!*/01 END

*!*	LOCAL lcFilePath

*!*	DO CASE
*!*		CASE UPPER(lcLocation) = 'L'
*!*			lcFilePath = goApp.cLocalPath
*!*		CASE UPPER(lcLocation) = 'S'
*!*			lcFilePath = goApp.cServerPath
*!*		CASE UPPER(lcLocation) = 'A'
*!*			lcFilePath = goApp.cAccountsPath
*!*		CASE UPPER(lcLocation) = 'R'
*!*			lcFilePath = goApp.cRemotePath
*!*		CASE UPPER(lcLocation) = 'H'
*!*			lcFilePath = goApp.cHomePath	
*!*		OTHERWISE
*!*			lcFilePath = goApp.cServerPath
*!*	ENDCASE

lcFilePath = ALLTRIM(lcFilePath) + lcTable

*!*	IF UPPER(lcAlias) = 'STOCK'
*!*		IF TYPE('nStockOpened') = 'U'
*!*			PUBLIC nStockOpened
*!*			nStockOpened = 0
*!*		ENDIF
*!*		nStockOpened = nStockOpened + 1
*!*		WAIT WINDOW "Opened: " + STR(nStockOpened) TIMEOUT .5
*!*	ENDIF

IF llExclusive
	USE (lcFilePath) IN (lnArea) ORDER (lcOrder) ALIAS (lcAlias) EXCL
ELSE
	USE (lcFilePath) IN (lnArea) ORDER (lcOrder) ALIAS (lcAlias)	
ENDIF
SELECT (lcAlias)

RETURN



FUNCTION CloseFile

LPARAMETERS lcTableName, lnArea

IF TYPE('lcTableName') = 'C' AND EMPTY(lcTableName) = .F.
	IF USED(lcTableName)
		USE IN (lcTableName)
		RETURN .T.
	ENDIF
ENDIF
IF TYPE('lnArea') = 'N' AND lnArea > 0
	USE IN (lnArea)
ENDIF	

RETURN .F.

	