#INCLUDE include/ces-settings.h

PROCEDURE CESErrorHandler
PARAMETER merror, mess, mess1, mprog, mlineno

	LOCAL llNewError
	
	llNewError = .T.
	
	DO CASE
		
		CASE mError = 1705
			RETURN .F.

		CASE INLIST(mError,1104,1105,1106) 
			return
			
		CASE mError = 1001 && Feature not available
			RETURN	
			
		CASE mError = 1943 && Member Node does not exist
			RETURN
			
		CASE mError = 1958 && Error Loading Printer Driver
			RETURN
				
		CASE mError = 1961 && Directory already exists
			RETURN
						
		CASE mError = 1712 && Field is a Duplicate
			RETURN
			
		CASE mError = 39 &&& Numeric Overflow
			RETURN
	ENDCASE
	
	goApp.nErrorCount = goApp.nErrorCount + 1

	*program stack
	lnprogram = PROGRAM(-1)
	lcProgram=goApp.cVersion
	FOR i = 1 TO lnprogram 
		lcProgram=lcProgram+CHR(13)+CHR(10)+REPLICATE(' ',i-1)+PROGRAM(i)
	ENDFOR
		
	lcErrorLogFileName = getErrorLogName()
	lcErrorLogFile =  goApp.cErrorLogPath + lcErrorLogFileName + '.log'
	
	STRTOFILE('The error on ' + DTOC(DATE()) + ' at ' + TIME() + CRLF,lcErrorLogFile,0)
	STRTOFILE('' + CRLF,lcErrorLogFile,1)
	STRTOFILE('Version: ' + goApp.cVersion + CRLF,lcErrorLogFile,1)
	STRTOFILE('' + CRLF,lcErrorLogFile,1)
	
	lcErrorDetails = ''
	lcErrorDetails = lcErrorDetails + 'Error number: ' + LTRIM(STR(merror)) + CRLF
	lcErrorDetails = lcErrorDetails + 'Error message: ' + mess  + CRLF
	lcErrorDetails = lcErrorDetails + 'Line number of error: ' + LTRIM(STR(mlineno))  + CRLF
	lcErrorDetails = lcErrorDetails + 'Program with error: ' + mprog + CRLF
	lcErrorDetails = lcErrorDetails + lcProgram + CRLF
	lcErrorCRCHash = SYS(2007,lcErrorDetails,0,1)

	STRTOFILE('Branch: ' + PADL(TRANSFORM(goSettings.nBranch),3,'0') + CRLF,lcErrorLogFile,1)
	STRTOFILE('' + CRLF,lcErrorLogFile,1)
	STRTOFILE('Error count: ' + TRANSFORM(goApp.nErrorCount) + CRLF,lcErrorLogFile,1)
	STRTOFILE('Error number: ' + LTRIM(STR(merror)) + CRLF,lcErrorLogFile,1)
	STRTOFILE('Error message: ' + mess  + CRLF,lcErrorLogFile,1)
	IF EMPTY(mess1) = .F.
		STRTOFILE('Line of code with error: ' + mess1  + CRLF,lcErrorLogFile,1)
	ENDIF
	STRTOFILE('Line number of error: ' + LTRIM(STR(mlineno))  + CRLF,lcErrorLogFile,1)
	STRTOFILE('Program with error: ' + mprog + CRLF,lcErrorLogFile,1) 
	STRTOFILE('' + CRLF,lcErrorLogFile,1)
	
	STRTOFILE(REPLICATE('-',80) + CRLF,lcErrorLogFile,1)
	STRTOFILE('Error CRC Code: ' + lcErrorCRCHash + CRLF,lcErrorLogFile,1)
	STRTOFILE(REPLICATE('-',80) + CRLF,lcErrorLogFile,1)	
	STRTOFILE('CALL STACK:' + CRLF,lcErrorLogFile,1)
	STRTOFILE(lcProgram + CRLF,lcErrorLogFile,1)

	STRTOFILE(REPLICATE('-',80) + CRLF,lcErrorLogFile,1)	
	STRTOFILE('TABLE INFO:' + CRLF,lcErrorLogFile,1)
	STRTOFILE("Current File: "+ TRANSFORM(DBF()) + CRLF,lcErrorLogFile,1)
	STRTOFILE("Current Alias: "+ TRANSFORM(ALIAS()) + CRLF,lcErrorLogFile,1)
	STRTOFILE("Current Work Area: "+ TRANSFORM(SELECT()) + CRLF,lcErrorLogFile,1)		
	STRTOFILE(REPLICATE('-',80) + CRLF,lcErrorLogFile,1)	
	
	STRTOFILE('ADDITIONAL INFORMATION:' + CRLF,lcErrorLogFile,1)
	STRTOFILE('' + CRLF,lcErrorLogFile,1)
	LIST MEMORY TO FILE (lcErrorLogFile) ADDITIVE NOCONSOLE
	STRTOFILE('' + CRLF,lcErrorLogFile,1)
	
	STRTOFILE(REPLICATE('-',80) + CRLF,lcErrorLogFile,1)	
	STRTOFILE('SYSTEM INFORMATION:' + CRLF,lcErrorLogFile,1)
	STRTOFILE('' + CRLF,lcErrorLogFile,1)
	LIST STATUS TO FILE (lcErrorLogFile) ADDITIVE NOCONSOLE
	STRTOFILE('' + CRLF,lcErrorLogFile,1)
	STRTOFILE(REPLICATE('=',80) + CRLF,lcErrorLogFile,1)	
	
	lcAdditionalDetails = ""
	IF FILE('supportdetails.txt')
		lcAdditionalDetails = FILETOSTR('supportdetails.txt') + CRLF + CRLF
	ENDIF

	*** Ok take a screen shot of the entire screen ***
	
	llShutDown = .F.
	
	lcErrorDetails = 'ERROR LOG: ' + CHR(13) + 'Version: ' + goApp.cVersion + CHR(13)
	
	lcEmailAddress = "" &&ALLTRIM(ThisForm.pgfMain.Page4.txtEmailAddress.Value)
	lcDealerMessage = "" &&ALLTRIM(ThisForm.pgfMain.Page4.edtErrorMessage.Value)
	lcDealerName = "" &&ALLTRIM(ThisForm.pgfMain.Page4.txtDealerName.Value)
	lcContactName = "" &&ALLTRIM(ThisForm.pgfMain.Page4.txtDealerContactNumber.Value)
		
	SELECT 0

	lcDealerInfoPath = goApp.cServerPath

	* let's try on the Server first
	IF FILE(lcDealerInfoPath + 'TouchDealerInfo.xml')
		TRY
			XMLTOCURSOR(lcDealerInfoPath + 'TouchDealerInfo.xml','tempDealerInfo',512)
		CATCH
		ENDTRY
	ELSE
		lcDealerInfoPath = goApp.cLocalPath
		IF FILE(lcDealerInfoPath + 'TouchDealerInfo.xml')
			TRY
				XMLTOCURSOR(lcDealerInfoPath + 'TouchDealerInfo.xml','tempDealerInfo',512)
			CATCH
			ENDTRY	
		ENDIF
	ENDIF	
	
	IF USED('tempDealerInfo')
		lcEmailAddress = ALLTRIM(tempDealerInfo.cEmail) &&ALLTRIM(ThisForm.pgfMain.Page4.txtEmailAddress.Value)
		lcDealerMessage = ALLTRIM(tempDealerInfo.cMessage) &&ALLTRIM(ThisForm.pgfMain.Page4.edtErrorMessage.Value)
		lcDealerName = ALLTRIM(tempDealerInfo.cName) &&ALLTRIM(ThisForm.pgfMain.Page4.txtDealerName.Value)
		lcContactName = ALLTRIM(tempDealerInfo.cPhone)
	ENDIF
	
	CloseFile('tempDealerInfo')
		
	lcText = "Company: " + goapp.ccompanyname + CHR(13) + "Version: v" + goApp.cVersion + " -- " + 'Branch: ' + PADL(TRANSFORM(goSettings.nBranch),3,'0') + CHR(13) + CHR(13)
	lcText = lcText + "We won't bother you with these details, however we thought we'd let you know what to do now."  	
		
	IF EMPTY(lcDealerMessage) = .F.	
		lcFromDealer = "FROM YOUR DEALER: " + CHR(13) + lcDealerMessage + CHR(13) + CHR(13)
	ELSE
		lcFromDealer = "We're sorry about this and we hope it didn't do it just when you really needed it." + CHR(13) + "So to help us make sure we fix this for you we've created a log of exactly what went wrong." + CHR(13) + CHR(13) + ;
			"Please make sure your Dealer gets informed of this so it can be corrected for you." + CHR(13) + CHR(13)
	ENDIF
	
	IF EMPTY(lcDealerName) = .F.
		lcFromDealer = lcFromDealer + CHR(13) + "Dealer Name: " + lcDealerName 
	ENDIF
	IF EMPTY(lcContactName) = .F.
		lcFromDealer = lcFromDealer + CHR(13) + "Contact Number: " + lcContactName
	ENDIF

	IF EMPTY(lcEmailAddress) = .F.
		lcFromDealer = lcFromDealer + CHR(13) + "Email Address: " + lcEmailAddress
	ENDIF

	lcMessage = lcText + CHR(13) + lcFromDealer + CHR(13) + CHR(13) + "Log details are here: " + CHR(13) + lcErrorLogFile

	local loTaskDialog
	* Show a message box so we can compare to the task dialog.
	
	newMessageBox(lcText + CHR(13) + CHR(13) + lcFromDealer,32,'It seems something has gone wrong..',,.T.,,,,.T.)		

	IF VERSION(2) != 2
		*sendErrorToServer(lcErrorLogFile, lcEmailAddress)
	ENDIF	
	
	IF llShutDown

		CLEAR events
		ON ERROR
		ON SHUTDOWN
		RELEASE ALL
		CANCEL
		QUIT		
		
	ENDIF

RETURN

FUNCTION getErrorLogName

	LOCAL lcErorLogName, lcDateTime  
	
	lcDateTime = TTOC(DATETIME())
	lcDateTime = STRTRAN(lcDateTime,'/','')
	lcDateTime = STRTRAN(lcDateTime,':','')
	lcDateTime = STRTRAN(lcDateTime,' ','_')
	lcDateTime = lcDateTime + '_' + STR(SECONDS(),5)
	
	lcVersion = STRTRAN(ALLTRIM(goApp.cVersion),".","")
	
	lcErorLogName = "Touch KVS Application " + lcVersion + '-' + lcDatetime
	
	RETURN lcErorLogName 

ENDFUNC


*!*	FUNCTION sendErrorToServer(lcFileToUpload, lcEmailAddress)

*!*	LOCAL loTouchLog AS "XML Web Service", lcResponse, lcFileContents
*!*	* LOCAL loTouchLog AS "MSSOAP.SoapClient30"
*!*	* Do not remove or alter following line. It is used to support IntelliSense for your XML Web service.
*!*	*__VFPWSDef__: loTouchLog = https://reg.cessoftware.co.uk/RegistrationsService/TouchLog.asmx?wsdl , TouchLog , TouchLogSoap
*!*	LOCAL loException, lcErrorMsg, loWSHandler
*!*	TRY
*!*		loWSHandler = NEWOBJECT("WSHandler",IIF(VERSION(2)=0,"",HOME()+"FFC\")+"_ws3client.vcx")
*!*		loTouchLog = loWSHandler.SetupClient("https://reg.cessoftware.co.uk/RegistrationsService/TouchLog.asmx?wsdl", "TouchLog", "TouchLogSoap")
*!*		* Call your XML Web service here.  ex: leResult = loTouchLog.SomeMethod()
*!*		
*!*		lcFileContents = FILETOSTR(lcFileToUpload)
*!*		
*!*		lcResponse = loTouchLog.UploadLogFileString(goApp.cVersion,goApp.nVolumeSerialNumber,JUSTFNAME(lcFileToUpload), lcFileContents, 0, .T., lcEmailAddress)
*!*		
*!*		IF lcResponse != "OK"
*!*			newMESSAGEBOX("Unable to send the Error log to the Error Log Server, the response returned was: " + lcResponse,16,"Error Logging")
*!*		ENDIF
*!*		
*!*	CATCH TO loException
*!*		lcErrorMsg="Error: "+TRANSFORM(loException.Errorno)+" - "+loException.Message
*!*		DO CASE
*!*		CASE VARTYPE(loTouchLog)#"O"
*!*			* Handle SOAP error connecting to web service
*!*		CASE !EMPTY(loTouchLog.FaultCode)
*!*			* Handle SOAP error calling method
*!*			lcErrorMsg=lcErrorMsg+CHR(13)+loTouchLog.Detail
*!*		OTHERWISE
*!*			* Handle other error
*!*		ENDCASE
*!*		* Use for debugging purposes
*!*		*newMESSAGEBOX(lcErrorMsg)
*!*	FINALLY
*!*	ENDTRY

*!*	RETURN
