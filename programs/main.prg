*!*	TRY

	IF VERSION(2) != 2
		_SCREEN.Visible = .F.
	ENDIF	

	CLEAR DLLS
	CLOSE DATABASES ALL

	*!*	IF FILE('newmem.txt')
	= SYS(3050, 1, VAL(SYS(3050, 1, 0)) / 3)
	= SYS(3050, 2, VAL(SYS(3050, 2, 0)) / 3)
	*!*	ENDIF

	local sL, bMin, nOEM
	SET CPDIALOG OFF
	CODEPAGE=852
	=CAPSLOCK(.F.)

	_SCREEN.Icon = 'GRAPHICS\ces.ico'
	_SCREEN.Caption = "CES Software Limited - Touch -> JSM Bar Bridge"

	SET DELETED ON
	SET century TO 19 ROLLOVER 20
	SET CURSOR ON
	SET status Bar OFF
	SET HEADING OFF
	SET AUTOSAVE ON
	SET DATE BRITISH
	set century On
	SET STATUS OFF
	SET EXCLUSIVE OFF
	SET ECHO OFF
	SET CONFIRM ON
	SET TALK OFF
	SET SCOREBOARD OFF
	SET ESCAPE OFF
	SET EXACT ON
	SET CONSOLE ON
	SET SAFETY OFF
	SET EXCLUSIVE OFF
	set hour to 24
	set seconds ON
	SET DELETE ON
	SET MULTILOCKS ON
	SET REPROCESS TO 30 SECONDS
	SET ENGINEBEHAVIOR 70
	_VFP.AutoYield = .T.

	IF _SCREEN.Visible 
		IF VERSION(2) != 2
			IF !FILE('debug.txt')
				SET sysmenu off
				SET MENU OFF
				ZOOM window screen max
				_SCREEN.MaxButton = .F.
				_SCREEN.MinButton = .F.
				_SCREEN.Closable = .F.
			ELSE
				SET SYSMENU OFF
				ZOOM window screen min
			ENDIF
		ELSE
			ZOOM window screen max
		ENDIF
*	ELSE
*		ZOOM window screen max
	ENDIF

	SET DECIMALS TO 4

	SET CLASSLIB TO classes\apicentralclasses, classes\apibasicclasses, classes\gdiplusx, classes\oskbutton, classes\dialogBoxes ADDITIVE
	
	SET PROCEDURE TO programs\programfunctions, programs\filehandling
	
	SET PROCEDURE TO programs\system.prg ADDITIVE

	DO programs\system	

	ON SHUTDOWN ShutDownEngine()

	* 10/02/2018 14:27:59 - JIRA(CI-970) - CR - Make sure the app only runs the once
	IF VERSION(2) != 2
		IF AppMutex() = .F. 
			MESSAGEBOX("The JSM Bar Bridge application is already running!",48,"JSM Bridge in use",5000)
		    RETURN .F.
		ENDIF
	ENDIF

	PUBLIC goApp, goSettings

	goApp = CREATEOBJECT("apicentralresource")
	IF VARTYPE(goApp) != 'O'
		EXIT
	ENDIF	
	
	IF goApp.setPaths() = .F.
		RETURN .F.
	ENDIF
	goApp.loadSettings()
	
	IF VERSION(2) != 2
		ON ERROR DO CESErrorHandler WITH ERROR( ), MESSAGE( ), MESSAGE(1), PROGRAM( ), LINENO( )
	ENDIF	
	
	LOCAL ARRAY arrVersion[1]

	AGETFILEVERSION(arrVersion, "jsmbarbridge.exe")

	IF TYPE('arrVersion[4]')='C'
		goApp.cversion = 'v'+ ALLTRIM(arrVersion[4])
	ENDIF
	
	DO FORM C:\Dev\JSMBarBridge\forms\frmT2IntMain
	READ EVENTS
	CLEAR EVENTS
	
CLOSE DATA ALL
CLEAR ALL
RELEASE ALL
** some test