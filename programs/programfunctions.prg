#INCLUDE include/ces-settings.h

PROCEDURE PlaySound(tcWavFile, tnFlags )
  * play synchronously (default) */
  #define SND_SYNC            0x0000  
  * play asynchronously */
  #define SND_ASYNC           0x0001  
  * silence (!default) if sound not found */
  #define SND_NODEFAULT       0x0002  
  * pszSound points to a memory file */
  #define SND_MEMORY          0x0004  
  * loop the sound until next sndPlaySound */
  #define SND_LOOP            0x0008  
  * don't stop any currently playing sound */
  #define SND_NOSTOP          0x0010  
  
  * don't wait if the driver is busy */
  #define SND_NOWAIT          0x00002000
  * name is a registry alias */
  #define SND_ALIAS           0x00010000
  * alias is a predefined ID */
  #define SND_ALIAS_ID          0x00110000
  * name is file name */
  #define SND_FILENAME        0x00020000
  * name is resource name or atom */
  #define SND_RESOURCE        0x00040004
  *#if(WINVER >= 0x0400)
  * purge non-static events for task */
  #define SND_PURGE           0x0040  
  * look for application specific association */
  #define SND_APPLICATION     0x0080  
  *#endif /* WINVER >= 0x0400 */

  DECLARE INTEGER PlaySound IN WinMM.dll AS PS_API ;
    STRING @ pszSound, INTEGER HMODULE_hmod, LONG DWORD_fdwSound  
  LOCAL lnFlags
  lnFlags = iif( vartype(tnFlags)='N', tnFlags, SND_ASYNC+SND_FILENAME )
  PS_API(tcWavFile,0,lnFlags)
ENDPROC

*** To run an External Application ***

FUNCTION ExternalProgram(lcFileToRun AS string, lcAction AS string, lcParameters AS string, lcWorkingFolder AS String, lnShowWin AS Integer)

IF VARTYPE(lcAction) != 'C'
	lcAction = 'open'
ENDIF	
IF VARTYPE(lcParameters) != 'C'
	lcParameters = ''
ENDIF	
IF VARTYPE(lcWorkingFolder) != 'C'
	lcWorkingFolder = ''
ENDIF	
IF VARTYPE(lnShowWin) != 'N'
	lnShowWin = 1
ENDIF	

DECLARE INTEGER ShellExecute IN shell32.dll ; 
  INTEGER hndWin, ; 
  STRING cAction, ; 
  STRING cFileName, ; 
  STRING cParams, ;  
  STRING cDir, ; 
  INTEGER nShowWin

cFileName = lcFileToRun
lnSuccess = ShellExecute(0,lcAction,cFileName,lcParameters,lcWorkingFolder,lnShowWin)

*WAIT WINDOW "Please Wait" TIMEOUT 2

IF lnSuccess < 32
	MESSAGEBOX("An Error Occured when trying to call the External Application (" + ALLTRIM(cFileName) + ")",48,"Error")
ENDIF

RELEASE ShellExecute

RETURN





FUNCTION OutputToFile (lcText, lcFileName, llAppend)

IF VARTYPE(lcFileName) != 'C'
	lcFileName = goApp.cTextFileName
ELSE
	goApp.cTextFileName = lcFileName	
ENDIF	

STRTOFILE(lcText + CHR(13) + CHR(10), lcFileName, llAppend)

RETURN


*** A simple little app that shuts down quietly ***
FUNCTION ShutDownEngine
ON SHUTDOWN
QUIT
RETURN



PROCEDURE CESErrorHandler
PARAMETER merror, mess, mess1, mprog, mlineno

	DO CASE
		CASE mError = 1001 && Feature not available
			RETURN	
			
		CASE mError = 1943 && Member Node does not exist
			RETURN
			
		CASE mError = 1958 && Error Loading Printer Driver
			RETURN
				
		CASE mError = 1961 && Directory already exists
			RETURN
			
		CASE mError = 1429 && Port is open/closed
			RETURN
			
		CASE mError = 1712 && Field is a Duplicate
			RETURN
			
		CASE mError = 39 &&& Numeric Overflow
			RETURN
			
	ENDCASE

	lcDateTime = TTOC(DATETIME())
	lcDateTime = STRTRAN(lcDateTime,'/','')
	lcDateTime = STRTRAN(lcDateTime,':','')
	lcDateTime = STRTRAN(lcDateTime,' ','_')
	lcDateTime = lcDateTime + '_' + STR(SECONDS(),5)

	lcErrorLogFile = goApp.cErrorLogPath + lcDateTime + '.log'
	lcErrorScreenShotName = goApp.cErrorLogPath + lcDateTime

	*** Ok take a screen shot of the entire screen ***
	*CaptureScreen(1,lcErrorScreenShotName)
	
	lcErrorDetails = 'Error number: ' + LTRIM(STR(merror)) + CHR(13)
	lcErrorDetails = lcErrorDetails + 'Error message: ' + mess + CHR(13)
	lcErrorDetails = lcErrorDetails + 'Line of code with error: ' + mess1 + CHR(13)
	lcErrorDetails = lcErrorDetails + 'Line number of error: ' + LTRIM(STR(mlineno)) + CHR(13)
	lcErrorDetails = lcErrorDetails + 'Program with error: ' + mprog 
	lcErrorDetails = lcErrorDetails + CHR(13) + CHR(13)
	lcErrorDetails = lcErrorDetails + '** PLEASE CONTACT YOUR DEALER ABOUT THIS ERROR **'
	
	IF goSettings.lAutomaticRestart = .F.
		MESSAGEBOX("An Error has happened, this was: " + CHR(13) + CHR(13) + lcErrorDetails,16,"Booker Promotions")
	ENDIF	

	lcCRLF = CHR(13) + CHR(10)
	
	STRTOFILE('Booker Promotions Admin App has errored on ' + DTOC(DATE()) + ' at ' + TIME() + lcCRLF,lcErrorLogFile,0)
	STRTOFILE('' + lcCRLF,lcErrorLogFile,1)
	STRTOFILE('Version: ' + goApp.cVersion + lcCRLF,lcErrorLogFile,1)
	STRTOFILE('' + lcCRLF,lcErrorLogFile,1)
	STRTOFILE('Error number: ' + LTRIM(STR(merror)) + lcCRLF,lcErrorLogFile,1)
	STRTOFILE('Error message: ' + mess  + lcCRLF,lcErrorLogFile,1)
	STRTOFILE('Line of code with error: ' + mess1  + lcCRLF,lcErrorLogFile,1)
	STRTOFILE('Line number of error: ' + LTRIM(STR(mlineno))  + lcCRLF,lcErrorLogFile,1)
	STRTOFILE('Program with error: ' + mprog + lcCRLF,lcErrorLogFile,1) 
	STRTOFILE('' + lcCRLF,lcErrorLogFile,1)
	STRTOFILE(REPLICATE('-',80) + lcCRLF,lcErrorLogFile,1)	
	STRTOFILE('ADDITIONAL INFORMATION:' + lcCRLF,lcErrorLogFile,1)
	STRTOFILE('' + lcCRLF,lcErrorLogFile,1)
	LIST MEMORY TO FILE (lcErrorLogFile) ADDITIVE NOCONSOLE
	STRTOFILE('' + lcCRLF,lcErrorLogFile,1)
	STRTOFILE(REPLICATE('-',80) + lcCRLF,lcErrorLogFile,1)	
	STRTOFILE('SYSTEM INFORMATION:' + lcCRLF,lcErrorLogFile,1)
	STRTOFILE('' + lcCRLF,lcErrorLogFile,1)
	LIST STATUS TO FILE (lcErrorLogFile) ADDITIVE NOCONSOLE
	STRTOFILE('' + lcCRLF,lcErrorLogFile,1)
	STRTOFILE(REPLICATE('=',80) + lcCRLF,lcErrorLogFile,1)

*!*		IF goSettings.lAutomaticRestart = .F.
		MESSAGEBOX('The Error Log file (' + lcErrorLogFile + ') has been created.' + CHR(13) + CHR(13) + 'This software will now close.',64,'Booker Promotions error')
*!*		ELSE	
*!*			goApp.oMainConsoleForm.AddToListBox("An error has occured, this is recorded in the log file: " + lcErrorLogFile)
*!*			goApp.oMainConsoleForm.AddToListBox("Closing down the link")
*!*			goApp.oMainConsoleForm.AddToListBox("---------------------")
*!*				
*!*			lcRestartFile = "restart.bat"
*!*			
*!*			STRTOFILE("@ECHO OFF" + lcCRLF,lcRestartFile,0)
*!*			STRTOFILE("@PING 127.0.0.1 -n 2 -w 4000 >nul" + lcCRLF,lcRestartFile,1)
*!*			STRTOFILE("@PING 127.0.0.1 -n %1% -w 4000 >nul" + lcCRLF,lcRestartFile,1)
*!*			STRTOFILE("@touchpayattablelink.exe" + lcCRLF,lcRestartFile,1)	
*!*			
*!*			ExternalProgram("restart.bat")
*!*		
*!*		ENDIF
		
	CLEAR EVENTS
	RELEASE ALL
	CANCEL
	QUIT
	
RETURN


FUNCTION CaptureScreen (lnType, lcFileName)
#include include/gpImage.h

gdip = CreateObject("gpInit")
img = CreateObject("gpImage")

DO CASE
	CASE lnType = 1
		*** Screen Shot ***
		WAIT WINDOW "Taking Screen Shot..." NOWAIT NOCLEAR
		img.Capture(0)
		img.SaveAsJPEG(lcFileName)
		WAIT CLEAR
	CASE lnType = 2
		*** Screen Shot of Focus Window ***
		Declare Long FindWindow in Win32API String ClassName, String WindowName
		wnd = FindWindow(NULL,"CES WaveSoft Console Error")
		img.Capture(wnd)
		img.SaveAsJPEG("lcFileName")
		
	CASE lnType = 3
		*** Screen Shot of the Windows ToolBar ***		
ENDCASE		

*!*	Declare Long FindWindow in Win32API String ClassName, String WindowName
*!*	wnd = FindWindow("Shell_TrayWnd", NULL)
*!*	img.Capture(wnd)
*!*	img.SaveAsJPEG("ShellTrayWnd")

*!*	wnd = FindWindow(NULL, "Microsoft Visual FoxPro")
*!*	img.Capture(wnd)
*!*	img.SaveAsJPEG("FoxProWnd")

RETURN




*-----------------------------------------------------------*
* Function: Get_Settings									*
* Author: Colin Richardson									*
*-----------------------------------------------------------*
* Returns the Settings for the passed Script				*
*-----------------------------------------------------------*
* PARAMETERS: lcScript - Script to locate					*
*			  lnType   - Data Type of Answer				*
*			  llServer - Local or Server Script File?		*
*-----------------------------------------------------------*

FUNCTION Get_Settings

LPARAMETERS lcScript, lnType, llServer

LOCAL luAnswer

luAnswer = NULL

*** Set the Default Answer ***
DO CASE
	CASE lnType = 1 
		luAnswer = .F.
	CASE lnType = 2 
		luAnswer = ""
	CASE lnType = 3 
		luAnswer = 0
	CASE lnType = 5 
		luAnswer = 0
	CASE lnType = 8
		LOCAL lnNumberOfDecimals
		lnNumberOfDecimals = SET('decimals')
		SET DECIMAL TO 6
		luAnswer = 0.000000
		SET DECIMAL TO lnNumberOfDecimals
	CASE lnType = 9 
		luAnswer = "None"
ENDCASE

*** Open up the Correct sys_info table ***
IF USED('sys_info')
	USE IN sys_info
ENDIF

IF llServer = .T.
	USE goApp.cServerPath + 'sys_info' ORDER Script IN 0
ELSE
	USE goApp.cLocalPath + 'sys_info' ORDER Script IN 0
ENDIF

*** Locate the Correct Script ***
IF SEEK(lcScript,"sys_info","script")
	DO CASE
		CASE sys_info.InfoType = lnType AND lnType = 1
			luAnswer = IIF(UPPER(ALLTRIM(sys_info.info)) = "YES",.T.,.F.)
			
		CASE sys_info.InfoType = lnType AND lnType = 2
			luAnswer =  ALLTRIM(sys_info.Info)
			
		CASE sys_info.InfoType = lnType AND lnType = 3
			luAnswer = INT(VAL(sys_info.info))
			
		CASE sys_info.InfoType = lnType AND lnType = 5
			luAnswer = VAL(sys_info.info)
			
		CASE sys_info.InfoType = lnType AND lnType = 8
			SET DECIMAL TO 6
			luAnswer = VAL(sys_info.info)
			SET DECIMAL TO 2
			
		CASE sys_info.InfoType = lnType AND lnType = 9
			luAnswer = ALLTRIM(sys_info.info)
	ENDCASE
ENDIF

USE IN sys_info

RETURN luAnswer



*-----------------------------------------------------------*
* Function: Update_Settings									*
* Author: Colin Richardson									*
*-----------------------------------------------------------*
* Updates the Value for the passed Script					*
*-----------------------------------------------------------*
* PARAMETERS: lcScript - Script to locate					*
*			  lcNewValue - New Value for this Script		*
*			  llServer - Local or Server Script File?		*
*-----------------------------------------------------------*

FUNCTION Update_Settings

LPARAMETERS lcScript, lcNewValue, llServer

*** Open up the Correct sys_info table ***
IF llServer = .T.
*	USE sServer + 'sys_info' ORDER Script IN 0
	USE goApp.cServerPath + 'sys_Info' ORDER Script IN 0
ELSE
*	USE sLocal + 'sys_Info' ORDER Script IN 0
	USE goApp.cLocalPath + 'sys_Info' ORDER Script IN 0
ENDIF

*** Locate the Correct Script ***
IF SEEK(lcScript,"sys_info")
	REPLACE info WITH lcNewValue IN sys_info
ENDIF

USE IN sys_info

RETURN 


FUNCTION AppMutex
    DECLARE INTEGER CreateMutex IN WIN32API INTEGER, INTEGER, STRING @
    DECLARE INTEGER CloseHandle IN WIN32API INTEGER
    DECLARE INTEGER GetLastError IN WIN32API
    DECLARE INTEGER SetProp IN WIN32API INTEGER, STRING @, INTEGER
    DECLARE INTEGER GetProp IN WIN32API INTEGER, STRING @
    DECLARE INTEGER RemoveProp IN WIN32API INTEGER, STRING @
    DECLARE INTEGER IsIconic IN WIN32API INTEGER
    DECLARE INTEGER SetForegroundWindow IN WIN32API INTEGER
    DECLARE INTEGER GetWindow IN WIN32API INTEGER, INTEGER
    DECLARE INTEGER ShowWindow IN WIN32API INTEGER, INTEGER
    DECLARE INTEGER GetDesktopWindow IN WIN32API
    DECLARE LONG FindWindow IN WIN32API LONG, STRING
    #DEFINE SW_RESTORE 9
    #DEFINE ERROR_ALREADY_EXISTS 183
    #DEFINE GW_HWNDNEXT 2
    #DEFINE GW_CHILD 5
    LOCAL llRetVal, lcExeFlag, lnExeHwnd, lnHwnd
    lcExeFlag = STRTRAN(_screen.caption, " ", "") + CHR(0)

    lnExeHwnd = CreateMutex(0, 1, @lcExeFlag)
    IF GetLastError() = ERROR_ALREADY_EXISTS
        lnHwnd = GetWindow(GetDesktopWindow(), GW_CHILD)
        DO WHILE lnHwnd > 0
            IF GetProp(lnHwnd, @lcExeFlag) = 1
                IF IsIconic(lnHwnd) > 0
                    ShowWindow(lnHwnd, SW_RESTORE)
                ENDIF
                SetForegroundWindow(lnHwnd)
                EXIT
            ENDIF
            lnHwnd = GetWindow(lnHwnd, GW_HWNDNEXT)
        ENDDO
        CloseHandle(lnExeHwnd)
        llRetVal = .F.
    ELSE
        SetProp(FindWindow(0, _screen.caption), @lcExeFlag, 1)
        llRetVal = .T.
    ENDIF
    RETURN llRetVal
ENDFUNC


FUNCTION WriteToLog(lcSection AS String, lcText AS String)

	lcLogFile = goApp.cLogPath + "JSMBarBridge_" + STRTRAN(DTOC(DATE()),'/','') + '.log'
		
	STRTOFILE(TRANSFORM(DATETIME()) + " " + lcSection + ": " + lcText + CHR(13) + CHR(10), lcLogFile,1)

RETURN



FUNCTION newMessageBox(lcMessage, lnOptions, lcTitle, lnAutoClose, llWarning, llInfo, llQuestion, llLeftAlign, llErrorMessage)
		
lnReturnValue = 0

IF VARTYPE(lcTitle) != "C"
	lcTitle = ""
ENDIF

DO CASE
	CASE llErrorMessage
		loMessageBox = CREATEOBJECT("frmErrorMessageBox",.F., lcMessage, lnOptions, lcTitle, lnAutoClose, llWarning, llInfo, llQuestion, llLeftAlign)
	OTHERWISE
		loMessageBox = CREATEOBJECT("frmMessageBox",.F., lcMessage, lnOptions, lcTitle, lnAutoClose, llWarning, llInfo, llQuestion, llLeftAlign)
ENDCASE
loMessageBox.show()

RETURN lnReturnValue

FUNCTION SignatureMessageBox(lcMessage, lnOptions, lcTitle, lnAutoClose, llWarning, llInfo, llQuestion, llLeftAlign, llErrorMessage)
		
lnReturnValue = 0

IF VARTYPE(lcTitle) != "C"
	lcTitle = ""
ENDIF

loMessageBox = CREATEOBJECT("frmSignatureMessageBox",.F., lcMessage, lnOptions, lcTitle, lnAutoClose, llWarning, llInfo, llQuestion, llLeftAlign)
loMessageBox.show()

RETURN lnReturnValue




FUNCTION WinApiErrMsg
LPARAMETERS tnErrorCode
#DEFINE FORMAT_MESSAGE_FROM_SYSTEM 0x1000
DECLARE Long FormatMessage IN kernel32 ;
Long dwFlags, Long lpSource, Long dwMessageId, ;
Long dwLanguageId, String @lpBuffer, ;
Long nSize, Long Arguments

LOCAL lcErrBuffer, lnNewErr, lnFlag, lcErrorMessage
lnFlag = FORMAT_MESSAGE_FROM_SYSTEM
lcErrBuffer = REPL(CHR(0),1000)
lnNewErr = FormatMessage(lnFlag, 0, tnErrorCode, 0, @lcErrBuffer,500,0)
lcErrorMessage = Transform(tnErrorCode) + " " + LEFT(lcErrBuffer, AT(CHR(0),lcErrBuffer)- 1 )
RETURN lcErrorMessage


FUNCTION IsRunning(tcTitle AS string)

DECLARE INTEGER GetActiveWindow IN Win32API
DECLARE INTEGER GetWindow IN Win32API INTEGER hWnd, INTEGER nType
DECLARE INTEGER GetWindowText IN Win32API INTEGER hWnd, STRING @cText, INTEGER nType

hNext = GetActiveWindow() && current app's window

* iterate through the open windows
DO WHILE hNext <> 0
	cText = REPLICATE(CHR(0),80)
	GetWindowText(hNext,@cText,80)

	IF EMPTY(UPPER(ALLTRIM(cText))) = .F. AND LEN(ALLTRIM(cText)) > 0
		STRTOFILE(ALLTRIM(cText) + CHR(13) + CHR(10),'c:\windowsopen.txt',1)
	ENDIF	

	&& get window title
	IF UPPER(ALLTRIM(tcTitle)) $ UPPER(cText) &&parameter text is present in window title
		EXIT
	ENDIF

	hNext = GetWindow(hNext,2) && next window
ENDDO

CLEAR DLLS "GetWindowText","GetActiveWindow","GetWindow"

RETURN hNext


FUNCTION isRunningNew 
	LPARAMETERS tcProcessName
	
	LOCAL llReturn as Boolean
	LOCAL loWMI, loProcesses
	
	loWMI = getobject("winmgmts:")
	loProcesses	= loWMI.ExecQuery([SELECT * FROM Win32_Process WHERE Name = '] + tcProcessName + ['])
	IF loProcesses.count > 0
		llReturn =.t.
	ELSE
		llReturn=.f.
	ENDIF
	
	RELEASE loWMI 
	RELEASE loProcesses
	RETURN llReturn
	
	
	
FUNCTION showPleaseWait
LPARAMETERS llClose, lcCaption

IF VARTYPE(lcCaption) != 'C'
	lcCaption = "Please Wait"
ENDIF

IF llClose = .F.
	PUBLIC goPleaseWait

	goPleaseWait = CREATEOBJECT('frmPleaseWait')

	goPleaseWait.lblMessage.Caption = lcCaption
	goPleaseWait.Show()

ELSE

	goPleaseWait.Release()
	
	RELEASE goPleaseWait

ENDIF


FUNCTION CloseFile

LPARAMETERS lcTableName, lnArea, lnOriginalArea

IF PCOUNT() < 3
	lnOriginalArea = 0
ENDIF

If VARTYPE(lcTableName) = 'C' And EMPTY(lcTableName) = .F.
	IF USED(lcTableName)
		USE IN (lcTableName)
		RETURN .T.
	ENDIF
ENDIF
IF VARTYPE(lnArea) = 'N' And lnArea > 0
	SELECT (lnArea)
	lcTableName=ALIAS()
	USE IN (lnArea)
ENDIF

IF lnOriginalArea > 0
	SELECT (lnOriginalArea)
ENDIF

RETURN .F.


FUNCTION WriteLog(lcSection AS String, lcText AS String)

	lcLogFile = ADDBS(SYS(5) + SYS(2003)) + 'logs\chipandpin\' + LEFT(TTOC(DATETIME(),1),8) + '.log'
	
	* create the directory if it doesn't exist
	IF DIRECTORY(ADDBS(SYS(5) + SYS(2003)) + 'logs\chipandpin') = .F.
		MKDIR(ADDBS(SYS(5) + SYS(2003)) + 'logs\chipandpin')
	ENDIF	
	
	STRTOFILE(TRANSFORM(DATETIME()) + " " + lcSection + " " + lcText + CHR(13) + CHR(10), lcLogFile,1)

RETURN


FUNCTION ClearOutOldLogs()

lnFiles = ADIR(laFiles,ADDBS(SYS(5) + SYS(2003)) + 'logs\JSM\*.log')

FOR lnLoop = 1 TO lnFiles
	IF laFiles(lnloop,3) < DATE() - 30
		DELETE FILE ADDBS(SYS(5) + SYS(2003)) + 'logs\JSM\' + laFiles(lnLoop,1)
	ENDIF
ENDFOR  
** Delete the update files
lnUpdateFiles = ADIR(laFiles,ADDBS(SYS(5) + SYS(2003)) + 'TouchToIntelligenceBridgeUpdater*.exe')
FOR lnLoop = 1 TO lnUpdateFiles
	DELETE FILE ADDBS(SYS(5) + SYS(2003)) + laFiles(lnLoop,1)
ENDFOR 

**

RETURN



FUNCTION isRunningNew 
LPARAMETERS tcProcessName

LOCAL llReturn as Boolean
LOCAL loWMI, loProcesses

loWMI = getobject("winmgmts:")
loProcesses	= loWMI.ExecQuery([SELECT * FROM Win32_Process WHERE Name = '] + tcProcessName + ['])
IF loProcesses.count > 0
	llReturn =.t.
ELSE
	llReturn=.f.
ENDIF

RELEASE loWMI 
RELEASE loProcesses
RETURN llReturn 